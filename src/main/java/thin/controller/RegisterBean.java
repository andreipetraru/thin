package thin.controller;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import thin.domain.User;
import thin.service.UserService;
import thin.util.EmailValidator;
import thin.util.Navigation;

import static org.apache.commons.lang.StringUtils.isEmpty;

/**
 * @author Andrei Petraru
 *         21 Feb 2015
 */
@Component("registerBean")
@Scope("request")
public class RegisterBean {
	@Autowired
	private UserService userService;
	@Autowired
	private UserSession userSession;

	private String firstName;
	private String lastName;
	private String password;
	private String email;
	private String error = "";

	public String register() {
		if (isEmpty(firstName) || isEmpty(lastName) || isEmpty(email) || isEmpty(password)) {
			error += "All fields must be completed";
			return null;
		}
		if (!EmailValidator.validate(email)) {
			error += " Invalid email";
			return null;
		}
		if (userService.findByEmail(email)) {
			error += " Email already exists";
			return null;
		}

		String sha1password = DigestUtils.sha1Hex(password);

		User user = new User(firstName, lastName, email, sha1password);
		userService.save(user);
		userSession.setLoggedInUser(user);

		return Navigation.ACTIONS;
	}

	public String getError() {
		return error;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
