package thin.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import thin.domain.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author Andrei Petraru
 *         17 Feb 2015
 */
@Service
@Transactional(readOnly = true)
public class UserService {
	@PersistenceContext
	private EntityManager entityManager;

	@Transactional(readOnly = false)
	public void save(User user) {
		entityManager.persist(user);
	}

	public boolean findByEmail(String email) {
		return entityManager.createQuery("SELECT u FROM User u WHERE u.email = :email")
				.setParameter("email", email)
				.getResultList().size() > 0;
	}

	public User findByLoginCredentials(String email, String password) {
		List<User> users = entityManager.createQuery("SELECT u FROM User u WHERE u.email = :email and u.password = :password")
				.setParameter("email", email)
				.setParameter("password", password)
				.getResultList();
		if (users.isEmpty()) {
			return null;
		}
		return users.get(0);
	}

	public List<User> getAll() {
		return entityManager.createQuery("SELECT u FROM User u").getResultList();
	}
}
