package thin.domain;

import org.springframework.context.annotation.Lazy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author Andrei Petraru
 *         21 Feb 2015
 */
@Entity
public class Meeting implements Serializable {
	@Id
	@GeneratedValue
	private Integer id;

	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;
	private String name;
	private String objective;
	@OneToMany(fetch = FetchType.EAGER)
	private List<User> participants;

	public Meeting() {
	}

	public Meeting(Date startDate, Date endDate, String name, String objective, List<User> participants) {
		this.startDate = startDate;
		this.endDate = endDate;
		this.name = name;
		this.objective = objective;
		this.participants = participants;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getObjective() {
		return objective;
	}

	public void setObjective(String objective) {
		this.objective = objective;
	}

	public List<User> getParticipants() {
		return participants;
	}

	public void setParticipants(List<User> participants) {
		this.participants = participants;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Meeting)) return false;

		Meeting meeting = (Meeting) o;

		if (!endDate.equals(meeting.endDate)) return false;
		if (!id.equals(meeting.id)) return false;
		if (!name.equals(meeting.name)) return false;
		if (!objective.equals(meeting.objective)) return false;
		if (!startDate.equals(meeting.startDate)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id.hashCode();
		result = 31 * result + startDate.hashCode();
		result = 31 * result + endDate.hashCode();
		result = 31 * result + name.hashCode();
		result = 31 * result + objective.hashCode();
		return result;
	}
}
