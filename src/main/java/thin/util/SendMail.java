package thin.util;

import org.springframework.stereotype.Component;
import thin.domain.Meeting;
import thin.domain.User;

import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * @author Andrei Petraru
 *         21 Feb 2015
 */

@Component
public class SendMail {
	// don't store user/pass in code
	final static String username = "USERNAME";
	final static String password = "PASSWORD";

	public static void send(Meeting meeting) {
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				});
		try {
			Message message = new MimeMessage(session);
			for (User user : meeting.getParticipants()) {
				message.setFrom(new InternetAddress("thin-project@gmail.com"));
				message.setRecipients(Message.RecipientType.TO,
						InternetAddress.parse(user.getEmail()));
				message.setSubject("New meeting");
				message.setText("Welcome to the new meeting, please follow the link \n"
						+ "http://localhost:8080/thin" + Navigation.MEETING + "/" + meeting.getId());
				Transport.send(message);
			}
		} catch (MessagingException e) {
			System.out.println("Email send error");
		}
	}
}
