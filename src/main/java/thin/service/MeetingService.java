package thin.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import thin.domain.Meeting;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author Andrei Petraru
 *         21 Feb 2015
 */
@Service
@Transactional(readOnly = true)
public class MeetingService {
	@PersistenceContext
	private EntityManager entityManager;

	@Transactional(readOnly = false)
	public void save(Meeting meeting) {
		entityManager.persist(meeting);
	}

	public List<Meeting> getAll() {
		return entityManager.createQuery("SELECT m FROM Meeting m").getResultList();
	}

	public Meeting findById(Integer id) {
		return entityManager.find(Meeting.class, id);
	}
}
