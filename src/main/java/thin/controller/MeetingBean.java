package thin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import thin.domain.Meeting;
import thin.service.MeetingService;

import java.io.Serializable;

/**
 * @author Andrei Petraru
 *         21 Feb 2015
 */

@Component
@Scope("request")
public class MeetingBean implements Serializable {
	@Autowired
	private MeetingService meetingService;

	private Integer id;
	private Meeting meeting;
	private String error;

	public void init() {
		if (id == null) {
			return;
		}

		meeting = meetingService.findById(id);
		if (meeting == null) {
			error = "Invalid id";
			return;
		}
	}

	public Meeting getMeeting() {
		return meeting;
	}

	public String getError() {
		return error;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}
