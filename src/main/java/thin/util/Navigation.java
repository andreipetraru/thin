package thin.util;

import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * @author Andrei Petraru
 *         21 Feb 2015
 */
@Component
public class Navigation implements Serializable {
	private static final String REDIRECT = "?faces-redirect=true";
	public static final String HOME = "/index.jsf" + REDIRECT;
	public static final String LOGIN = "/pages/login.jsf" + REDIRECT;
	public static final String REGISTER = "/pages/register.jsf" + REDIRECT;
	public static final String ACTIONS = "/pages/actions.jsf" + REDIRECT;
	public static final String MEETING = "/pages/meeting.jsf" + REDIRECT;

	public String getHome() {
		return HOME;
	}

	public String getLogin() {
		return LOGIN;
	}

	public String getRegister() {
		return REGISTER;
	}

	public String getActions() {
		return ACTIONS;
	}

	public String getMeeting() {
		return MEETING;
	}
}
