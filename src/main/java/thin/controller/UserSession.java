package thin.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import thin.domain.User;
import thin.util.Navigation;

import java.io.Serializable;

/**
 * @author Andrei Petraru
 *         21 Feb 2015
 */

@Component
@Scope("session")
public class UserSession implements Serializable {
	private User loggedInUser;

	public String logout() {
		loggedInUser = null;
		return Navigation.HOME;
	}

	public User getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(User loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	public boolean isLoggedIn() {
		return loggedInUser != null;
	}

}
