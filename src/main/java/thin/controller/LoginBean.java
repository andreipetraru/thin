package thin.controller;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import thin.domain.User;
import thin.service.UserService;
import thin.util.EmailValidator;
import thin.util.Navigation;

import javax.annotation.PostConstruct;

import static org.apache.commons.lang.StringUtils.isEmpty;

/**
 * @author Andrei Petraru
 *         21 Feb 2015
 */
@Component
@Scope("request")
public class LoginBean {
	@Autowired
	private UserService userService;
	@Autowired
	private UserSession userSession;

	private String password;
	private String email;
	private String error = "";

	@PostConstruct
	public void init() {
		if (userSession.isLoggedIn()) {
			return;
		}
	}

	public String login() {
		if (isEmpty(email) || isEmpty(password)) {
			error += "All fields must be completed";
			return null;
		}
		if (!EmailValidator.validate(email)) {
			error += " Invalid email";
			return null;
		}
		String sha1password = DigestUtils.sha1Hex(password);
		User user = userService.findByLoginCredentials(email, sha1password);
		if (user == null) {
			error += " Invalid username or password";
			return null;
		}
		userSession.setLoggedInUser(user);

		return Navigation.ACTIONS;
	}

	public String getError() {
		return error;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
