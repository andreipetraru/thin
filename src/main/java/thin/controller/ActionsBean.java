package thin.controller;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.DualListModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import thin.domain.Meeting;
import thin.domain.User;
import thin.service.MeetingService;
import thin.service.UserService;
import thin.util.SendMail;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * @author Andrei Petraru
 *         21 Feb 2015
 */

@Component
@Scope("view")
public class ActionsBean {
	@Autowired
	private UserSession userSession;
	@Autowired
	private MeetingService meetingService;
	@Autowired
	private UserService userService;
	@Autowired
	private SendMail sendMail;

	private Date startDate;
	private Date endDate;
	private String name;
	private String objective;
	private DualListModel<User> participants;
	private List<User> source;
	private List<User> target = new ArrayList<>();
	private String error = "";
	private String success = "";
	private List<Meeting> meetings;
	private List<Meeting> myMeetings = new ArrayList<>();

	@PostConstruct
	public void init() {
		if (!userSession.isLoggedIn()) {
			return;
		}
		source = userService.getAll();
		source.remove(userSession.getLoggedInUser());
		participants = new DualListModel<>(source, target);
		meetings = meetingService.getAll();
		for (Meeting meeting : meetings) {
			if (meeting.getParticipants().contains(userSession.getLoggedInUser())) {
				myMeetings.add(meeting);
			}
		}
	}

	public void create() {
		clear();
		if (isEmpty(name) || isEmpty(objective) || startDate == null || endDate == null) {
			error += "All fields must be completed";
			return;
		}
		Meeting meeting = new Meeting(startDate, endDate, name, objective,
				Arrays.asList(userSession.getLoggedInUser()));
		meetingService.save(meeting);
		sendMail.send(meeting);
		success = "Meeting created";
	}

	private void clear() {
		error = "";
		success = "";
	}

	public void onDateSelect(SelectEvent event) {
		startDate = (Date) event.getObject();
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getObjective() {
		return objective;
	}

	public void setObjective(String objective) {
		this.objective = objective;
	}

	public DualListModel<User> getParticipants() {
		return participants;
	}

	public void setParticipants(DualListModel<User> participants) {
		this.participants = participants;
	}

	public String getError() {
		return error;
	}

	public String getSuccess() {
		return success;
	}

	public List<Meeting> getMeetings() {
		return meetings;
	}

	public List<Meeting> getMyMeetings() {
		return myMeetings;
	}
}
