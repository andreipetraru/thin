package thin.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Andrei Petraru
 *         21 Feb 2015
 */
public class EmailValidator {
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private static final Pattern pattern = Pattern.compile(EMAIL_PATTERN);

	public static boolean validate(String email) {
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();

	}
}

